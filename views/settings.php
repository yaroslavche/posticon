<div class="wrap">
    <h1>Post Icon</h1>
    <form method="post" action="options.php">
	    <?php
        settings_fields('posticon_settings_group');
        do_settings_sections('posticon_settings');
        submit_button();
        ?>
    </form>
</div>
