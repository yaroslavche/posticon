<?php

declare(strict_types=1);

namespace PostIcon;

final class PostIconAdmin
{
    private static $initiated = false;
    private static $icons = ['menu', 'admin-site', 'dashboard', 'admin-post', 'admin-media', 'admin-links', 'admin-page', 'admin-comments', 'admin-appearance', 'admin-plugins', 'admin-users', 'admin-tools', 'admin-settings', 'admin-network', 'admin-home', 'admin-generic', 'admin-collapse', 'welcome-write-blog', 'welcome-add-page', 'welcome-view-site', 'welcome-widgets-menus', 'welcome-comments', 'welcome-learn-more', 'format-aside', 'format-image', 'format-gallery', 'format-video', 'format-status', 'format-quote', 'format-chat', 'format-audio', 'camera', 'images-alt', 'images-alt2', 'video-alt', 'video-alt2', 'video-alt3', 'image-crop', 'image-rotate-left', 'image-rotate-right', 'image-flip-vertical', 'image-flip-horizontal', 'undo', 'redo', 'editor-bold', 'editor-italic', 'editor-ul', 'editor-ol', 'editor-quote', 'editor-alignleft', 'editor-aligncenter', 'editor-alignright', 'editor-insertmore', 'editor-spellcheck', 'editor-distractionfree', 'editor-kitchensink', 'editor-underline', 'editor-justify', 'editor-textcolor', 'editor-paste-word', 'editor-paste-text', 'editor-removeformatting', 'editor-video', 'editor-customchar', 'editor-outdent', 'editor-indent', 'editor-help', 'editor-strikethrough', 'editor-unlink', 'editor-rtl', 'align-left', 'align-right', 'align-center', 'align-none', 'lock', 'calendar', 'visibility', 'post-status', 'edit', 'trash', 'arrow-up', 'arrow-down', 'arrow-right', 'arrow-left', 'arrow-up-alt', 'arrow-down-alt', 'arrow-right-alt', 'arrow-left-alt', 'arrow-up-alt2', 'arrow-down-alt2', 'arrow-right-alt2', 'arrow-left-alt2', 'sort', 'leftright', 'list-view', 'exerpt-view', 'share', 'share-alt', 'share-alt2', 'twitter', 'rss', 'facebook', 'facebook-alt', 'googleplus', 'networking', 'hammer', 'art', 'migrate', 'performance', 'wordpress', 'wordpress-alt', 'pressthis', 'update', 'screenoptions', 'info', 'cart', 'feedback', 'cloud', 'translation', 'tag', 'category', 'yes', 'no', 'no-alt', 'plus', 'minus', 'dismiss', 'marker', 'star-filled', 'star-half', 'star-empty', 'flag', 'location', 'location-alt', 'vault', 'shield', 'shield-alt', 'search', 'slides', 'analytics', 'chart-pie', 'chart-bar', 'chart-line', 'chart-area', 'groups', 'businessman', 'id', 'id-alt', 'products', 'awards', 'forms', 'portfolio', 'book', 'book-alt', 'download', 'upload', 'backup', 'lightbulb', 'smiley'];
    private static $positions = ['left', 'right'];

    public static function init() : void
    {
        if (!static::$initiated) {
            static::initHooks();
        }
    }

    public static function initHooks() : void
    {
        add_action('admin_init', [static::class, 'admin_init']);
        add_action('admin_menu', [static::class, 'admin_menu'], 5);
        add_filter('plugin_action_links', [static::class, 'plugin_action_links'], 10, 2);
        static::$initiated = true;
    }

    public static function admin_init()
    {
        load_plugin_textdomain('posticon');

        register_setting('posticon_settings_group', 'posticon_options', [static::class, 'sanitizeOptions']);

        add_settings_section('section_id', __('Main settings', 'posticon'), '', 'posticon_settings');

        add_settings_field('icon', __('Icon', 'posticon'), [static::class, 'icon_field'], 'posticon_settings', 'section_id');
        add_settings_field('icon_position', __('Icon position', 'posticon'), [static::class, 'icon_position_field'], 'posticon_settings', 'section_id');
        add_settings_field('posts', __('Posts', 'posticon'), [static::class, 'posts_field'], 'posticon_settings', 'section_id');
    }

    public static function sanitizeOptions($input)
    {
        $newInput = [];
        if (isset($input['icon']) && in_array($input['icon'], static::$icons)) {
            $newInput['icon'] = $input['icon'];
        }
        if (isset($input['icon_position']) && in_array($input['icon_position'], static::$positions)) {
            $newInput['icon_position'] = $input['icon_position'];
        }
        $posts = $input['posts'] ?? [];
        if (is_array($posts)) {
            $posts = array_map('intval', $posts);
            static::setAllDisabled();
            foreach ($posts as $postID) {
                update_post_meta($postID, '_posticon_set', 'true');
            }
        }
        return $newInput;
    }

    private static function setAllDisabled()
    {
        $posts = get_posts([
            'numberposts' => 0,
            'category'    => 0,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post',
            'suppress_filters' => true,
        ]);
        foreach ($posts as $post) {
            setup_postdata($post);
            update_post_meta($post->ID, '_posticon_set', '');
        }
        wp_reset_postdata();
    }

    public static function icon_field()
    {
        $options = get_option('posticon_options');
        $selectedIcon = $options['icon'] ?? '';
        foreach (static::$icons as $icon) {
            printf('<label><input type="radio" name="posticon_options[icon]" value="%s"%s/><span class="dashicons dashicons-%s"></span></label>&ensp;', $icon, $icon === $selectedIcon ? ' checked="checked"' : '', $icon);
        }
        return; ?>
	    <select name="posticon_options[icon]">
		    <option value=""<?php empty($selectedIcon) ? ' selected="selected' : ''; ?>>None</option>
		    <?php
            foreach (static::$icons as $icon) {
                printf('<option value="%s"%s>%s</option>', $icon, $icon === $selectedIcon ? ' selected="selected"' : '', $icon);
            } ?>
	    </select>
	    <?php
    }

    public static function icon_position_field()
    {
        $options = get_option('posticon_options');
        $iconPosition = $options['icon_position'] ?? 'left';
        if (!in_array($iconPosition, static::$positions)) {
            $iconPosition = 'left';
        }
        foreach (static::$positions as $position) {
            printf('<label><input type="radio" name="posticon_options[icon_position]" value="%s"%s/>%s</label>&ensp;', $position, $position === $iconPosition ? ' checked="checked"' : '', $position);
        }
    }

    public static function posts_field()
    {
        $posts = get_posts([
            'numberposts' => 0,
            'category'    => 0,
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post',
            'suppress_filters' => true,
        ]);
        foreach ($posts as $post) {
            setup_postdata($post);
            $posticonSet = get_post_meta($post->ID, '_posticon_set', true) === 'true';
            printf('<label><input type="checkbox" name="posticon_options[posts][]" value="%s"%s/>%s</label>&ensp;', $post->ID, $posticonSet === true ? ' checked="checked"' : '', $post->post_title);
        }
        wp_reset_postdata();
    }

    public static function plugin_action_links($links, $file)
    {
        if ($file === 'posticon/posticon.php') {
            $links[] = sprintf('<a href="%s">%s</a>', esc_url(static::get_page_url()), __('Settings', 'posticon'));
        }
        return $links;
    }

    public static function admin_menu()
    {
        if (class_exists('Jetpack')) {
            add_action('jetpack_admin_menu', [static::class, 'load_menu']);
        } else {
            static::load_menu();
        }
    }

    public static function load_menu()
    {
        if (class_exists('Jetpack')) {
            add_submenu_page('jetpack', __('Post Icon', 'posticon'), __('Post Icon', 'posticon'), 'manage_options', 'posticon_settings', [static::class, 'showSettingsPage']);
        } else {
            add_options_page(__('Post Icon', 'posticon'), __('Post Icon', 'posticon'), 'manage_options', 'posticon_settings', [static::class, 'showSettingsPage']);
        }
    }

    public static function showSettingsPage()
    {
        $viewTemplateFile = sprintf('%sviews/%s.php', POSTICON_PLUGIN__PLUGIN_DIR, 'settings');
        if (file_exists($viewTemplateFile)) {
            require_once $viewTemplateFile;
        } else {
            throw new \RuntimeException('Settings view not found');
        }
    }

    public static function get_page_url()
    {
        $args = array('page' => 'posticon_settings');
        $url = add_query_arg($args, class_exists('Jetpack') ? admin_url('admin.php') : admin_url('options-general.php'));
        return $url;
    }
}
