<?php

declare(strict_types=1);

namespace PostIcon;

final class PostIcon
{
    private static $initiated = false;
    private static $icon = null;
    private static $iconPosition = null;

    public static function init()
    {
        if (!static::$initiated) {
            static::initHooks();
        }
    }

    public static function onActivation()
    {
    }

    public static function onDeactivation()
    {
    }

    public static function initHooks()
    {
        add_filter('the_title', [static::class, 'the_title'], 10, 2);
        static::$initiated = true;
    }

    public static function the_title($title, $id = null)
    {
        if (null === $id) {
            return $title;
        }
        $hasPostIcon = get_post_meta($id, '_posticon_set', true) === 'true';
        if (!$hasPostIcon) {
            return $title;
        }
        if (null === static::$icon) {
            $options = get_option('posticon_options');
            static::$icon = $options['icon'] ?? '';
            static::$iconPosition = $options['icon_position'] ?? 'left';
        }
        if (empty(static::$icon)) {
            return $title;
        }
        $newTitle = '';
        if (static::$iconPosition !== 'left') {
            $newTitle = sprintf('%s <span class="dashicons dashicons-%s"></span>', $title, static::$icon);
        } else {
            $newTitle = sprintf('<span class="dashicons dashicons-%s"></span> %s', static::$icon, $title);
        }
        return $newTitle;
    }
}
