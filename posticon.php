<?php
/**
 * @package           PostIcon
 *
 * @wordpress-plugin
 * Plugin Name:       Post Icon
 * Plugin URI:        http://bitbucket.org/yaroslavche/posticon
 * Description:       Show icon in post title
 * Version:           0.1.0
 * Author:            yaroslavche
 * Author URI:        http://github.com/yaroslavche
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       posticon
 */

use PostIcon\PostIcon;
use PostIcon\PostIconAdmin;

if (! defined('WPINC')) {
    exit;
}

// todo: move to class constants
define('POSTICON_PLUGIN_VERSION', '0.1.0');
define('POSTICON_PLUGIN__MINIMUM_WP_VERSION', '4.0');
define('POSTICON_PLUGIN__PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once POSTICON_PLUGIN__PLUGIN_DIR . 'includes/PostIcon.php';

register_activation_hook(__FILE__, [PostIcon::class, 'onActivation']);
register_deactivation_hook(__FILE__, [PostIcon::class, 'onDeactivation']);

add_action('init', [PostIcon::class, 'init']);

if (is_admin()) {
    require_once POSTICON_PLUGIN__PLUGIN_DIR . 'includes/PostIconAdmin.php';
    add_action('init', [PostIconAdmin::class, 'init']);
}
